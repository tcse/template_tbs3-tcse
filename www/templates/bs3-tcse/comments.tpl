<p></p>
{* <div class="row"> *}

	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

		<div class="row">
			[aviable=lastcomments]
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<h3 style="margin-bottom: 0.4em;"><a href="{news-link}-id-{id}">{news-title}</a></h3>
			</div>
			[/aviable]
			<div class="hidden-xs col-sm-1 col-md-1 col-lg-1">
				<img src="{foto}" class="img-responsive" alt="" />
			</div>
			<div class="col-xs-12 col-sm-11 col-md-11 col-lg-11">
				<div>
					<small>
						<a href="#comment-id-{id}" title="постоянная ссылка на комментарий"><span class="badge">#{id}</span></a> написал: <strong>{author}</strong><br />
						Группа: {group-name}<br />
						{date} {ip}
					</small>
				</div>	
			</div>

		</div>

		<p>
			<div class="well smartcomments">
				{comment}
				
				[rating]
					<br>
					[rating-type-1]
					<div class="pull-right">
						<div class="rate">{rating}</div>
					</div>
					[/rating-type-1]

					[rating-type-2]
					<div class="pull-right">
						<ul class="list-inline">
							<li>[rating-plus]<i class="fa fa-thumbs-o-up"></i>[/rating-plus]</li>
							<li>{rating}</li>
						</ul>
					</div>
					[/rating-type-2]

					[rating-type-3]
					<div class="pull-right">
						<ul class="list-inline">
							<li>[rating-minus]<i class="fa fa-thumbs-o-down"></i>[/rating-minus]</li>
							<li>{rating}</li>
							<li>[rating-plus]<i class="fa fa-thumbs-o-up"></i>[/rating-plus]</li>
						</ul>
					</div>
					[/rating-type-3]
					<br>
				[/rating]

			</div>

		    	[signature]
			<div class="visible-sm visible-xs">
				<br><br>--------------------<br>
				{signature}
			</div>
			[/signature]
		</p>
		<ul class="list-inline">
			<li>{mass-action}</li>
			<li>[reply]<button type="button" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top"  title="ответить на этот комментарий"><i class="fa fa-reply"></i> <span class="hidden-xs">ответить</span></button>[/reply]</li>
			<li>[fast]<button type="button" class="btn btn-info btn-xs"data-toggle="tooltip" data-placement="top"  title="цитировать"><i class="fa fa-quote-left"></i></button>[/fast]</li>
			<li>[complaint]<button type="button" class="btn btn-default  btn-xs"><i class="fa fa-thumbs-down"></i> жалоба</button>[/complaint]</li>
			<li>[com-edit]<button type="button" class="btn btn-warning  btn-xs"><i class="fa fa-edit"></i> редактировать</button>[/com-edit]</li>
			<li>[com-del]<button type="button" class="btn btn-danger  btn-xs"><i class="fa fa-times-circle"></i> удалить</button>[/com-del]</li>
			
		</ul>


	</div>

{* </div> *}


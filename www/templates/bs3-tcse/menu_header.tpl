<nav class="navbar {* navbar-inverse *} shadow-menu" role="navigation" data-spy="affix" data-offset-top="160" data-offset-bottom="200">
    <div class="container">
        
        {* Блок кнопок в шапке меню на малых экранах *}
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#LoginModal">
				<i class="fa fa-ellipsis-v"></i>
			</button>

            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#SeachModal">
				<i class="fa fa-search"></i>
			</button>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<i class="fa fa-bars"></i>
			</button>

            <a class="navbar-brand" href="/">BS3-TCSE</a>
        </div> {* /Блок кнопок в шапке меню на малых экранах *}

        {* Блюк пунктов меню схлопывающихся на малых экранах *}
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-info"> </i> О компании<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://tcse-cms.com/contacts.html">Страница контактов</a></li>
                        <li><a href="http://tcse-cms.com/index.php?do=feedback">Форма обратной связи</a></li>
                        <li><a href="mailto:mail@tcse-cms.com">Написать email</a></li>
                        <li><a href="tel:+78123092624">Позвонить +7 (812) 309-26-24</a></li>
                        <li class="divider"></li>
                        <li><a href="/?do=lastcomments">Лента комментариев</a></li>

                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder"></i> Разделы сайта<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://tcse-cms.com/portfolio/">Портфолио</a></li>
                        <li><a href="http://tcse-cms.com/works/">Продукты</a></li>
                        <li><a href="http://tcse-cms.com/project/">Проекты</a></li>
                        <li><a href="http://tcse-cms.com/docs/">Инструкции</a></li>
                        <li class="divider"></li>
                        <li><a href="http://tcse-cms.com/price.html">Цены</a></li>
                        <li><a href="http://tcse-cms.com/main/tcse-cms/427-uslugi-i-rascenki.html">Услуги</a></li>
                        <li><a href="/abonent.html">Абонентское обслуживание</a></li>
                        <li><a href="/after-sales.html">Постпродажная поддержка</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Оформить заказ<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="http://tcse-cms.com/zakaz-anketa.html">Заполнить анкету</a></li>
                        <li><a href="http://tcse-cms.com/uploads/brif_sait_TCSE-CMS.doc">Бриф на разработку сайта</a></li>
                        <li><a href="http://tcse-cms.com/uploads/brif_design_TCSE-CMS.doc">Бриф на дизайн-макета сайта</a></li>
                        <li><a href="http://tcse-cms.com/zakaz-smeta.html">Оценочная смета</a></li>


                    </ul>
                </li>
            </ul>

            {* Форма поиска по сайту *}
            <form action="" name="searchform" method="post" id="s_form" class="navbar-form pull-right visible-md visible-lg" role="search">
                <div class="form-group">
                    <input type="hidden" name="do" value="search" />
                    <input type="hidden" name="subaction" value="search" />
                    <input class="form-control" placeholder="поиск по сайту" name="story" value="" type="text" id="story" />
                </div>
                <input class="btn btn-default" value="Найти" type="submit" />
            </form> {* /Форма поиска по сайту *}

        </div> {* / Блюк пунктов меню схлопывающихся на малых экранах *}

    </div>
</nav>

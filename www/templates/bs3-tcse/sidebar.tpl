<div class="panel-group" id="accordion">

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSB01"><i class="fa fa-list"></i> Меню сайта</a></h4>
		</div>
		<div id="collapseSB01" class="panel-collapse collapse [not-aviable=showfull]in[/not-aviable]">
			<div class="panel-body">
			<a href="/?do=lastcomments" class="btn btn-success btn-block btn-xs">Все комментарии</a>
			<a href="/index.php?do=feedback" class="btn btn-success btn-block btn-xs">Обратная связь</a>
			<a href="/about.html" class="btn btn-success btn-block btn-xs">О сайте</a>
			</div>
		</div>
	</div>


	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSB03"><i class="fa fa-line-chart"></i> ТОП-10</a></h4>
		</div>
		<div id="collapseSB03" class="panel-collapse collapse">
			
			<ul class="list-group">
				{topnews}
			</ul>
		</div>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion" href="#collapseSB05"><i class="fa fa-calendar"></i> Архив публикаций</a></h4>
		</div>
		<div id="collapseSB05" class="panel-collapse collapse">
			<div class="panel-body">
				<ul class="list-unstyled">
					{archives}
				</ul>
			</div>
		</div>
	</div>

</div>

<div class="panel panel-info">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a href="#" data-toggle="modal" data-target="#LoginModal">
				[not-group=5]<i class="fa fa-user"></i> Профиль [/not-group]
				[group=5]<i class="fa fa-sign-in"></i> Авторизация[/group]
			</a>
		</h4>
	</div>
</div>

<div class="panel panel-primary">
	<div class="panel-heading">
		<h4 class="panel-title">
			<a data-toggle="collapse" data-parent="#accordion" href="#collapseSB04">
				<i class="fa fa-commenting-o" aria-hidden="true"></i> Комментарии 
				<span class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i></span>
			</a>
		</h4>

	</div>
	<div id="collapseSB04" class="panel-collapse collapse in">
		<div class="list-group">
			{customcomments category="2,3,5-8" template="custom/comlist" available="global" from="0" limit="10" order="date" sort="desc"}
		</div>
	</div>
</div>
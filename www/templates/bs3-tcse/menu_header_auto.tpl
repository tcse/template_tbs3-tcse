<nav class="navbar shadow-menu" role="navigation" data-spy="affix" data-offset-top="160" data-offset-bottom="200" id="primary_nav_wrap">
    <div class="container">
        {* Блок кнопок в шапке меню на малых экранах *}
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#LoginModal">
				<i class="fa fa-ellipsis-v"></i>
			</button>

            <button type="button" class="navbar-toggle" data-toggle="modal" data-target="#SeachModal">
				<i class="fa fa-search"></i>
			</button>

            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<i class="fa fa-bars"></i>
			</button>

            <a class="navbar-brand" href="/">BS3-TCSE</a>
        </div> {* /Блок кнопок в шапке меню на малых экранах *}

        {* Блюк пунктов меню схлопывающихся на малых экранах *}
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {* Выводит меню из категорий сайта. Оформление меню осуществляется в шаблоне categorymenu.tpl *}
            {catmenu}
        </div> {* / Блюк пунктов меню схлопывающихся на малых экранах *}

    </div>
</nav>

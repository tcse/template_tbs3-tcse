<div class="panel panel-default">
    <div class="panel-heading">
        <h1 class="panel-title">{description}</h1>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="full-content">{static}</div>
                [print-link]<i class="fa fa-print"></i> Распечатать[/print-link]
                <div align="center">{pages}</div>
            </div>
        </div>
    </div>
</div>
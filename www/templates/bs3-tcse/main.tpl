<!DOCTYPE html>
<html>

<head>
    {* mobile tags *}
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    {headers} 
    {* Favicons *}
    <link rel="shortcut icon" href="/favicon.ico"> 
    {* Bootstrap *}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
    {* DLE style *}
    <link rel="stylesheet" href="{THEME}/css/engine.css">
    <link rel="stylesheet" href="{THEME}/css/styles.css">
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    {* Логотип сайта *}
                    <div class="header-logo">
                        <a href="/"><img src="http://tcse-cms.com/templates/tcse2015/assets/images/logo1.png" class="img-responsive" alt="logo site"></a>
                    </div> {* / Логотип сайта *}
                </div>
            </div>
        </div>
        {* Главное меню в шапке сайта *} 
        {include file="menu_header.tpl"} {* / Главное меню в шапке сайта *}

        {* Альтернатива Главное меню в шапке сайта *} 
            {* {include file="menu_header_auto.tpl"}  *}
        {* / Альтернатива меню в шапке сайта *}
    </header>

    <main>
        <div class="container">
            {speedbar}
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
                    {info}
                    <div class="row">
                        {content}
                    </div>
                </div>
                {* Боковая колонка (сайдбар) *}
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                    {include file="sidebar.tpl"}
                </div> {* / Боковая колонка (сайдбар) *}
            </div>
        </div>
    </main>

    <footer>
        {* Блок меню в подвале сайта *} 
        {include file="menu_footer.tpl"} {* /Блок меню в подвале сайта *}
        <div class="container">
            <div class="row">
                {* Блок копирайтов в подвале сайта *}
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <p>&emsp;</p>
                    <p>
                        © 2017 Бесплатный шаблон <b>BS3-TCSE</b> для <a href="http://dle-news.ru">DataLife Engine v11.2</a> от веб-студии <a href="http://tcse-cms.com">tcse-cms.com</a>.
                    </p>
                    <p>
                        В основе фреймворк <a href="http://getbootstrap.com/css/" target="_blank">twitter bootstrap 3</a> и набор иконок <a href="http://fontawesome.io/icons/" target="_blank">fontawesome</a>.
                    </p>
                    <p>&emsp;</p>
                </div> {* /Блок копирайтов в подвале сайта *}
            </div>
        </div>
    </footer>


    {* Модальное окно формы авторизации на сайте *}
    <div class="modal fade" id="LoginModal" tabindex="-1" role="dialog" aria-labelledby="LoginModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>
                    <h4 class="modal-title">Форма авторизации</h4>
                </div>
                <div class="modal-body">
                    {login}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal"><i class="fa fa-home"></i> Ясно</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Понятно</button>
                </div>
            </div>
        </div>
    </div> {* / Модальное окно формы авторизации на сайте *} 

    {* Модальное окно формы поиска на сайте *}
    <div class="modal fade" id="SeachModal" tabindex="-1" role="dialog" aria-labelledby="SeachModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="form-inline" name="searchform" role="form" method="post" action="/">
                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="do" value="search">
                            <input type="hidden" name="subaction" value="search">
                            <input type="text" class="form-control input-lg" id="focusedInput" name="story" placeholder="Введите поисковый запрос...и нажмите Enter">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> {* / Модальное окно формы поиска на сайте *} 
    
    {* Bootstrap JavaScript *} 
    {jsfiles} 
    {AJAX}
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="{THEME}/js/libs.js"></script>

</body>

</html>

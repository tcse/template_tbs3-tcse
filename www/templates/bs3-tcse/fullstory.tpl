
	<article class="clearfix">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
		          <h1>{title}</h1>
		          <p>
				<i class="fa fa-folder-open-o"></i> {link-category}&emsp;
				<i class="fa fa-calendar"></i> [day-news]{date}[/day-news]&emsp;
				<i class="fa fa-eye"></i> {views}&emsp;
				[add-favorites]<i class="fa fa-bookmark-o"></i>[/add-favorites]
				[del-favorites]<i class="fa fa-bookmark"></i>[/del-favorites]&emsp;
				[edit]<i class="fa fa-edit"></i>[/edit]
		          </p>
	          	
			<div class="full-content clearfix">
				{full-story}
			</div>

		          	[rating]
				<div class="block-rating">
					<ul class="list-inline">
						<li class="text-danger">Оцените публикацию</li>
						<li>
							[rating-type-1]
							<div class="rate">{rating}</div>
							[/rating-type-1]

							[rating-type-2]
							<ul class="list-inline">
								<li>[rating-plus]<i class="fa fa-thumbs-o-up"></i>[/rating-plus]</li>
								<li>{rating}</li>
							</ul>
							[/rating-type-2]

							[rating-type-3]
							<ul class="list-inline">
								<li>[rating-minus]<i class="fa fa-thumbs-o-down"></i>[/rating-minus]</li>
								<li>{rating}</li>
								<li>[rating-plus]<i class="fa fa-thumbs-o-up"></i>[/rating-plus]</li>
							</ul>
							[/rating-type-3]
						</li>
					</ul>
				</div>
			[/rating]
	         	
	         		<p>
	         			
	         			<div class="panel panel-default">
			  		<div class="panel-body">
	         					[tags]<i class="fa fa-tags"></i> <span class="tagcloud2">{tags}</span>&emsp;&emsp;[/tags]
	         					[complaint]<i class="fa fa-thumbs-o-down"></i>  Настрочить жалобу в спортлото [/complaint]&emsp;&emsp;
	         					<i class="fa fa-user"></i> {author}&emsp;&emsp;
	         					[print-link]<i class="fa fa-print"></i>  Распечатать[/print-link]
	         				</div>
	         			</div>
	         			
	         		</p>
	         		  	
		</div>
	</article>


	<section class="hidden-xs">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="panel panel-warning">
				<div class="panel-heading">Реклама на сайте</div>
				<div class="panel-body">
					<p>
						сюда можно поставить код контекстной рекламы.
					</p>
				</div>
			</div>	
		</div>
	</section>

	[related-news]
	<section>
		<div class="col-lg-12 clearfix">
			<h3>Похожие публикации</h3>
			<ul class="list-group">
				{related-news}
			</ul>
		</div>
	</section>
	[/related-news]
	
	



	<section>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			[comments]<h3>Обсуждения</h3>[/comments]

			[not-comments]
			<div class="well">
				У данной публикации еще нет комментариев. Хотите начать обсуждение?
			</div>
			[/not-comments]


		  	[group=5]
			<h3>Вы не авторизованы!</h3>
			<p class="well">
				Обратите внимание, если вы не авторизуетесь, то Ваш комментарий перед публикацией обязательно будет отправлен на модерацию. <br>
				Рекомендуем вам <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#LoginModal">войти под своим логином</a><br>
				Или используйте авторизацию через соц.сети<br>
				[vk]<noindex><a href="{vk_url}" target="_blank" class="btn btn-default"><i class="fa fa-vk"></i> Вконтакте</a></noindex>[/vk]
				[facebook]<noindex><a href="{facebook_url}" target="_blank" class="btn btn-default"><i class="fa fa-facebook"></i> Facebook</a></noindex>[/facebook]
			</p>
			[/group]

		  	<div>
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li class="active"><a href="#ComDLE" data-toggle="tab">Добавить комментарий</a></li>
					<li><a href="#ComVK" data-toggle="tab">Вконтакте</a></li>
					<li><a href="#ComFB" data-toggle="tab">Фейсбук</a></li>
					<li><a href="#ComTW" data-toggle="tab">Твиттер</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content comment-style-block">
					<div class="tab-pane fade in active" id="ComDLE">
						<p><br></p>
						<p>
							{addcomments}
						</p>
						
					</div>
					<div class="tab-pane fade" id="ComVK">
						<p><br></p>
						<p>Сюда можно вставить <a href="https://vk.com/dev/Comments" target="_blank">код виджета vk.com</a></p>
					</div>
					<div class="tab-pane fade" id="ComFB">
						<p><br></p>
						<p>Сюда можно вставить код виджета <a href="https://developers.facebook.com/docs/plugins/comments" target="_blank">Facebook</a> или <a href="https://disqus.com/" target="_blank">Disqus</a>.</p>
					</div>
					<div class="tab-pane fade" id="ComTW">
						<p><br></p>
						<p>Сюда можно вставить код <a href="https://twitter.com/settings/widgets/new/user" target="_blank">Twitter</a> виджета</p>
						<p>
							<noindex>
							<a class="twitter-timeline" href="https://twitter.com/tcsecms" data-widget-id="426987062743400448">Твиты пользователя @tcsecms</a>
							<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
							</noindex>
						</p>
					</div>
				</div>
			</div>

		  	<p>{comments}</p>
		  	<p>{navigation}</p>
		</div>
	</section>
